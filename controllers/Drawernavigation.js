//import liraries
import React, { Component } from 'react';
import { Text, Image } from 'react-native';
import { DrawerNavigator, DrawerItems } from 'react-navigation';
import Profile from '../components/profile/Profile';
import EditProfile from '../components/profile/EditProfile';
import Login from '../components/login/Login';
import ForgotPassword from '../components/login/ForgotPassword';
import Logout from '../components/login/Logout';
import Deliveries from '../components/deliveries/Deliveries';
import StartDelivery from '../components/deliveries/StartDelivery';
import DeliveryCompleted from '../components/deliveries/DeliveryCompleted';
import { Container, Header, Body, Content, View } from 'native-base';

const CustomDrawerComponent = (props)=> (

    <Container>
        <Header style={{height:200, backgroundColor:'#C00A27'}}>
            <Body style={{alignItems:'center', justifyContent:"center"}}>
                <Image source={require('../src/user.png')} style={{width: 120, height:120}} />
                <Text style={{justifyContent: 'center', alignSelf: 'center', color: '#fff', fontSize: 20}}>Bosun Jones</Text>
            </Body>
        </Header>
        <Content>
            <DrawerItems {...props} />
        </Content> 
        <View style={{position:'absolute', bottom:10, alignItems:'center', flex:1, alignSelf: 'center' }}>
          <Image source={require('../src/favicon.png')} style={{width:30, height:30, resizeMode:'contain'}} />
        </View>   
    </Container>
)

const AppDrawernavigation = DrawerNavigator({

    Deliveries: {screen: Deliveries},
    Profile: {screen: Profile},
    EditProfile: {screen: EditProfile},
    Login: {screen: Login},
    Logout: {screen: Logout},
    ForgotPassword: {screen: ForgotPassword},
    StartDelivery: {screen: StartDelivery},
    DeliveryCompleted: {screen: DeliveryCompleted},
},
{
    initialRouteName:'Login',
    contentComponent:CustomDrawerComponent,
    contentOptions:{
        activeTintColor: '#000',
    }
});

AppDrawernavigation.navigationOptions = {
    header: null
}


export default AppDrawernavigation;
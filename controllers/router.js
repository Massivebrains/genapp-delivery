import React from 'react';

import {
    StackNavigator,
    DrawerNavigator,
  } from "react-navigation";


import Login from '../components/login/Login';
import Drawernavigation from './Drawernavigation'


 const BRouter = StackNavigator({
	
	Login: { screen: Login },
  Drawernavigation: { screen: Drawernavigation },
   
 },{
    navigationOptions:{
      gesturesEnabled: false
    }
 });



export default BRouter;

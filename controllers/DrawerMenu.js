import React from "react";
import {
    View,
    Text,
    StyleSheet,
    Platform
} from "react-native";

import Icon from 'react-native-vector-icons/Ionicons'

const DrawerMenu = (props) => (
    <View style={[{ padding: 5, top:9 }, Platform.OS == 'android' ? styles.iconContainer : null]}>
        <Icon name="ios-menu" size={25} style={{color:'#fff'}} onPress={() => props.navigation.navigate('DrawerOpen')}/>
    </View>
)

export default DrawerMenu;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconContainer: {
        paddingTop: 10, marginRight: 5
    }
})
import React from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';
import { Container } from 'native-base';

export default class Spinner extends React.Component {

    constructor(props) {

        super(props);
    }

    render() {

        return (

            <Container>
                <ActivityIndicator animating={true} size={50} style={styles.indicator} color='#D60B2B' />
            </Container>
        );
    }
}


const styles = StyleSheet.create({

    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: '100%'
    }
});
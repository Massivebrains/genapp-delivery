import React, { Component } from 'react';
import { View, Text, StyleSheet,Image,ScrollView,TouchableOpacity } from 'react-native';
import { Icon } from 'native-base'
import API from '../API';
import Utils from '../Utils';
import Local from '../Local';

export default class DeliveryItems extends Component {

    static navigationOptions = { header: null }
  
    constructor(props) {

        super(props);
        this.state = { orders: [] };
    }

    async componentDidMount() {

        if(this.props.orders) this.setState({orders : this.props.orders});
    }

    accept = async (order_id) => {

        try {

            let user = await Local.get('user');
            let response = await API.get(`deliveries/accept/${order_id}`, user.api_token);
            
            if(response.status == false){

                Utils.toast(response.data);

            }else{

                let orders = await API.get('rider/orders', user.api_token);
                this.setState({orders : orders.data});
                Utils.toast(response.data);
            }

        } catch (ex) {

            Utils.toast();
        }
    }

    start = async order => {

        this.props.navigation.navigate('StartDelivery', {order : order});
    }

    render() {

        return(
            <ScrollView contentContainerStyle={{ flexGrow: 1, backgroundColor: 'rgba(255,255,255,0)', marginTop:10, marginBottom:50 }}>
                
                {this.state.orders.map(row => {

                    return (

                        <View style={styles.catBox} key={row.id}>

                            <View style={styles.imageContainer}>
                                <TouchableOpacity onPress={() => {this.start(row)}}>
                                    <Image style={ styles.imageStyle} source={require('../../src/delivery-avatar.png')}/>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.ProductDetails}>
                                <Text style={styles.ProductName}>Order #{row.order_reference}</Text>
                                <Text style={styles.ProductPrice}>{row.order_details.length} Item(s)  |  6 Miles</Text>
                                <Text style={styles.address}><Icon name="ios-pin" style={{color:'#D60B2B', fontSize:14}}/> {row.address}</Text>
                                
                                <TouchableOpacity style={[styles.Badge, {display : row.delivery_status == 'not_accepted' ? 'flex' : 'none'}]} onPress={() => {this.accept(row.id)}}>
                                    <Text style={styles.textAccept}>ACCEPT DELIVERY</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    );
                })}

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container:{
        paddingTop:5,
        margin:5,
        marginBottom:20,
        flex:1,
    },
    imageStyle:{
        width:150,
        height:150,
        // resizeMode:'contain',
        // alignSelf: "flex-start",
        marginLeft: 10,
    },
    Btn:{
        padding:15,
        backgroundColor:'#D60B2B',
        borderRadius:50,
        position:'relative',
        marginTop:10,
    },
    address:{
        color:'#808080',
        fontSize:12,
        marginBottom:7
    },
    BtnTxt:{
        color:'#fff',
        textAlign:'center',
    },
    ProductName:{
        fontWeight:'700',
        marginBottom:5,
        fontSize:16,
    },
    catBox:{
        margin:8,
        flex:1,
        flexDirection:'row',
        backgroundColor:'#fff',
        borderWidth:2,
        borderColor:'#ccc',
        borderRadius:15,
        position:'relative',
        padding:5,
        marginBottom:10,
        overflow:'hidden'
    },
    imageContainer:{
        flex:1,
        flexDirection:'column'
    },
    ProductDetails:{
        flex:1,
        flexDirection:'column',
        // paddingLeft: 20,
        // textAlign:"center",
        // paddingRight: 10,
        // paddingTop:5,
    },
    ProductPrice:{
        fontWeight:"600",
        color:"#454545",
        fontSize:13,
        marginBottom:3
    },
    Badge:{
        borderColor:'#D60B2B',
        borderWidth:1,
        backgroundColor:'#D60B2B',
        paddingTop:8,
        paddingBottom: 8,
        borderRadius:50,
        marginTop:3,
        marginBottom:4
    },
    BadgeCompleted:{
        borderColor:'#659C35',
        borderWidth:1,
        padding:5,
        borderRadius:50,
        marginTop:15
    },
    textAccept:{
        color: '#ffffff',
        paddingLeft: 5,
        paddingRight: 5,
        fontSize:12,
        textAlign:"center"
    }
});
import React, { Component } from 'react';
import { View, Text, StyleSheet,Image } from 'react-native';
import { Header } from 'react-native-elements';
import { Tab, Tabs } from 'native-base';
import DrawerMenu from '../../controllers/DrawerMenu';
import DeliveryItems from '../../components/deliveries/DeliveryItems';
import API from '../API';
import Utils from '../Utils';
import Spinner from '../Spinner';
import Local from '../Local';

export default class Deliveries extends Component {

    static navigationOptions = {

        drawerLabel: 'Deliveries', drawerIcon: () => ( <Image source={require('../../src/delivery-icon.png')} style={[styles.icon, {tintColor: '#D60B2B'}]} /> ),
    };

    constructor(props) {

        super(props);
  
        this.state = { orders: [], loaded : false };
    }

    async componentDidMount() {

        this.init();
    }
    
    init = async () => {

        try {

            let user = await Local.get('user');
            let response = await API.get('rider/orders', user.api_token);

            if(response.status == false){

                Utils.toast(response.data);
                this.setState({ loaded : true });

            }else{

                this.setState({ orders: response.data, loaded : true });
            }

        } catch (ex) {

            Utils.toast();
            this.setState({loaded : true});
        }
    }
  
  render() {
    
    if (this.state.loaded == false) return (<Spinner title='Deliveries' navigation={this.props.navigation} />);

    return(

        <View style={styles.MainBg}>

            <Header statusBarProps={{ barStyle: 'light-content' }} outerContainerStyles={{ backgroundColor: '#D60B2B' }} leftComponent={ <DrawerMenu navigation={ this.props.navigation }/>} centerComponent={{ text: 'Deliveries', style: { color: '#fff' } }} />

            <Tabs tabBarUnderlineStyle={{borderBottomWidth:5, borderColor:'#D60B2B'}}>
                
                <Tab heading="Available" tabStyle={{backgroundColor: '#ffffff',}} textStyle={{color: '#D60B2B'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#D60B2B', fontWeight: '700'}}>
                    <DeliveryItems orders={this.state.orders} navigation={ this.props.navigation }/>
                </Tab>

                <Tab heading="Recent" tabStyle={{backgroundColor: '#ffffff',}} textStyle={{color: '#D60B2B'}} activeTabStyle={{backgroundColor: '#fff'}} activeTextStyle={{color: '#D60B2B', fontWeight: '700'}}>
                    <View style={styles.noDelivery}>
                        <Image source={require('../../src/no-delivery.png')} style={styles.imageDelivery}/>
                        <Text style={{fontSize:25, textAlign:"center", color:'#454545'}}>No Delivery Has{"\n"}Been Made Recently</Text>
                    </View>
                </Tab>

            </Tabs>

        </View>
    );
  }
}

const styles = StyleSheet.create({

    noDelivery:{
        alignItems:'center',
        justifyContent: 'center',
        flex:1,
    },
    imageDelivery:{
        alignItems:'center',
        justifyContent: 'center',
        marginBottom: 10,
        width:120,
        height:120,
        resizeMode:'contain'
    },
    MainBg:{
        backgroundColor:'#f1f1f1',
        flex:1,
    },
    icon: {
        width: 20,
        height: 27.64,
      },
});
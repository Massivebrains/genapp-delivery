import React, { Component } from 'react';
import { ScrollView, View, StyleSheet,TouchableOpacity, Text, Image } from 'react-native';
import { Header } from 'react-native-elements';
import { Tab, Tabs, Card, Content, CardItem, Body, Icon, List, ListItem, Left, Right } from 'native-base';
import MapView from 'react-native-maps';
import API from '../API';
import Utils from '../Utils';
import Local from '../Local';

export default  class StartDelivery extends Component {

    static navigationOptions = {  header: null, drawerLabel: () => null }

    constructor(props) {

        super(props);

        this.state = { order: {}, details : [] };
    }

    async componentDidMount() {

        let order = this.props.navigation.getParam('order');
        if(order)  this.setState({order : order});

        if(order.order_details) this.setState({details : order.order_details});
    }

    finish = async () => {

        try {

            let user = await Local.get('user');
            let order_id = this.state.order.id;

            console.log(order_id);
            
            let response = await API.get(`deliveries/finish/${order_id}`, user.api_token);
            
            if(response.status == false){

                Utils.toast(response.data);

            }else{

                this.props.navigation.navigate('DeliveryCompleted');
                Utils.toast(response.data);
            }

        } catch (ex) {

            Utils.toast();
        }
    }

  render() {
      
    return(

        <View style={styles.MainBg}>

            <Header statusBarProps={{ barStyle: 'light-content' }} outerContainerStyles={{ backgroundColor: '#D60B2B' }} leftComponent={{ icon: 'arrow-back', color: '#fff', paddingTop:5, onPress: () => this.props.navigation.navigate('Deliveries') }} centerComponent={{ text: `Delivery (Order #${this.state.order.order_reference})`, style: { color: '#fff' } }} />
        
            <ScrollView contentContainerStyle={{ flexGrow: 1, backgroundColor: '#F3F3F3'}}>

                <View style={styles.Container}>
                    <View>
                        <MapView style={styles.map} initialRegion={{ latitude: 4.8398801, longitude: 6.9858311, latitudeDelta: 0.1, longitudeDelta: 0.1 }}>
                    </MapView>
                    </View>

                    <View style={styles.DeliveryStatus}>
                        <Text>{this.state.order.delivery_status}</Text>
                    </View>

                    <View style={styles.DeliveryInfo}>

                        <View style={styles.DeliveryInfoTrigger}>
                            <Text style={{fontWeight:'600', paddingTop:7}}>Journey not started yet.</Text>
                        </View>

                        <View style={styles.DeliveryInfoTrigger}>
                            <TouchableOpacity style={styles.DeliveryTriggerStart} onPress={() => {alert('Delivery has Started')}}>
                                <Text style={{color:'#fff', fontSize:11, textAlign:"center"}}>START DELIVERY</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                    <Tabs tabBarUnderlineStyle={{borderBottomWidth:5, borderColor:'#D60B2B'}}>
                        <Tab heading="INFO" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#D60B2B'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#D60B2B', fontWeight: 'normal'}}>

                            <View style={styles.DeliveryBoxLeft}>
                                <Text style={{fontWeight:'600'}}>ADDRESS:</Text>
                                <Text>{this.state.order.address}</Text>
                                <Text style={{fontWeight:'600', marginTop:20}}>EXTRA NOTE:</Text>
                                <Text>{this.state.order.notes}</Text>
                            </View>

                            <View style={{flexDirection:'row', alignContent:'center', justifyContent:'center', backgroundColor:'#f3f3f3', paddingBottom:40, width:'100%'}}>
                                <TouchableOpacity style={styles.btn} onPress={() => {this.finish()}}>
                                    <Text style={{color:'#fff', alignSelf:'center', marginLeft: 10, marginRight: 10}}><Icon name="ios-checkmark-circle" style={{color:'#ffffff', fontSize:16}}/> FINISH DELIVERY</Text>
                                </TouchableOpacity>
                            </View> 
                            
                        </Tab>

                        <Tab heading="ORDER DETAILS" tabStyle={{backgroundColor: '#fff'}} textStyle={{color: '#D60B2B'}} activeTabStyle={{backgroundColor: '#fff'}} activeTextStyle={{color: '#D60B2B', fontWeight: 'normal'}}>

                            <View style={styles.DeliveryBoxRight}>

                            <Card style={{borderRadius:15, overflow:"hidden"}}>
                                <CardItem header bordered>
                                    <Text style={{fontWeight:'bold'}}>Order Details (#{this.state.order.order_reference})</Text>
                                </CardItem>

                                <CardItem bordered>
                                    <Content>
                                        <List>
                                            {this.state.details.map(row => {

                                                return (

                                                    <ListItem thumbnail key={row.id}>
                                                        <Left><Text>{row.name}</Text></Left>
                                                        <Body><Text style={{color:'#D60B2B'}}>x{row.quantity}</Text></Body>
                                                        <Right><Text style={{color:'#D60B2B', fontWeight:'bold'}}>{row.total}</Text></Right>
                                                    </ListItem>
                                                )
                                            })}
                                            
                                        </List>

                                    </Content>
                                </CardItem>
                                </Card>

                                <List>
                                    <ListItem thumbnail>
                                        <Left>
                                        <Text style={{fontWeight:'bold'}}>Total:</Text>
                                        </Left>
                                        <Body style={{float:"right"}}>
                                        <Text style={{color:'#D60B2B', fontWeight:'bold'}}>{this.state.order.total}</Text>
                                        </Body>
                                    </ListItem>
                                </List>

                                <View style={styles.orderInfo}>
                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:'bold'}}>Payment Mode:</Text>
                                    <Text>{this.state.order.payment_method}</Text>
                                </View>

                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:"bold"}}>Date Ordered:</Text>
                                    <Text>{this.state.order.created_at}</Text>
                                </View>

                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:"bold"}}>Order Status:</Text>
                                    <Text style={{color:"#D60B2B"}}>{this.state.order.status}</Text>
                                </View>

                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:"bold"}}>Shipping Address:</Text>
                                    <Text>{this.state.order.address}</Text>
                                </View>

                            </View>

                            <View style={{flexDirection:'row', alignContent:'center', justifyContent:'center', backgroundColor:'#f3f3f3', paddingBottom:40, width:'100%'}}>
                                <TouchableOpacity style={styles.btn} onPress={() => {this.finish(this.state.order.id)}}>
                                    <Text style={{color:'#fff', alignSelf:'center', marginLeft: 10, marginRight: 10}}><Icon name="ios-checkmark-circle" style={{color:'#ffffff', fontSize:16}}/> FINISH DELIVERY</Text>
                                </TouchableOpacity>
                            </View> 
                            
                            </View>

                        </Tab>
                    </Tabs> 
                    
                </View>
            </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({

    map:{
        // flex:1,
        height:400,
        top:0,
        left:0,
        bottom:0,
        right:0,
    },
    DeliveryBoxLeft:{
        padding:20,
        marginTop:10,
        paddingBottom:20,
        backgroundColor:'#f1f1f1'
    },
    DeliveryBoxRight:{
        padding:20,
        marginTop:10,
        backgroundColor: '#f1f1f1',
    },
    orderInfo:{
        margin:10,
    },
    infoDetails:{
        marginBottom:20,
    },
    DeliveryInfo:{
        flexDirection:'row',
        width:'100%',
        textAlign:"center",
        padding:15,
        backgroundColor:'#fff',
        borderBottomColor: '#cccccc',
        borderTopColor: '#ffffff',
        borderLeftColor: '#ffffff',
        borderRightColor: '#ffffff',
        borderWidth: 1,
    },
    DeliveryInfoTrigger:{
        flexDirection:'column',
        textAlign:"center"
    },
    DeliveryTriggerStart:{
        padding:10,
        paddingLeft:20,
        paddingRight:20,
        backgroundColor:'#C00A27',
        borderRadius:50,
        marginLeft:30,
        marginRight: 'auto',
        flex:1,
    },
    DeliveryStatus:{
        textAlign:'center',
        alignItems:'center',
        padding:12,
        borderBottomColor: '#cccccc',
        borderTopColor: '#ffffff',
        borderLeftColor: '#ffffff',
        borderRightColor: '#ffffff',
        borderWidth: 1,
        width:'100%',
        flex:1,
        backgroundColor:"#ffffff"
    },
    MainBg:{
        backgroundColor:'#f3f3f3',
        flex:1,
    },
    Container:{
        flex:1,
        alignItems:'center',
    },
    PickerSize:{
      width: 250,
      paddingTop:10,
      paddingBottom:10,
      marginLeft:5,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      borderWidth:2,
      borderColor:'#C00A27',
      borderRadius:3,
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.10,
      shadowRadius: 13.16,
      elevation: 10,
      borderBottomColor: '#ffffff',
    },
    ProductImageContainer:{
      flex:5,
      height: 200,
    },
    ProductImage:{
      resizeMode: 'contain',
      height: 300
    },
    btn:{
      textAlign:'center',
      padding:15,
      backgroundColor:'#C00A27',
      borderRadius:50,
      width:'70%'
  },
    buttoncounter:{
      borderColor:'#C00A27', 
      borderRadius:50,
      paddingLeft: 5, 
      paddingRight: 5, 
      borderWidth: 2,
      marginLeft: 'auto',
      flexDirection:'row',
      paddingTop:10,
      height:45
  },
  countext:{
    color:'#D60B2B',
    fontSize: 20,
    paddingLeft:15,
    paddingRight:15,
},
countexts:{
    color:'#C00A27',
    fontSize: 18,
    marginLeft: 10,
    marginRight:10,
},
});
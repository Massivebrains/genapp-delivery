import React, { Component } from 'react';
import { ScrollView, View, StyleSheet,TouchableOpacity, Text, Image } from 'react-native';
import { Header } from 'react-native-elements';
import DrawerMenu from '../../controllers/DrawerMenu';
import { Card, Content, CardItem, Body, List, ListItem, Left, Right } from 'native-base';

export default  class DeliveryCompleted extends Component {

static navigationOptions = {
    header: null,
    drawerLabel: () => null
}

  render() {
      
  var {navigate} = this.props.navigation;

    return(
        <View style={styles.MainBg}>
            <Header
            statusBarProps={{ barStyle: 'light-content' }}
            outerContainerStyles={{ backgroundColor: '#D60B2B' }}
            leftComponent={ <DrawerMenu navigation={ this.props.navigation }/>}
            centerComponent={{ text: 'Order #2405 (Completed!)', style: { color: '#fff' } }}
            // rightComponent={ <ShoppingCart navigation={ this.props.navigation }/>}
            />
        
            <ScrollView contentContainerStyle={{ flexGrow: 1, backgroundColor: '#F3F3F3'}}>
                <View style={styles.Container}>

                    <View style={styles.ProductImageContainer}>
                        <Image style={styles.ProductImage} source={require('../../src/completed.png')}/>

                        <Text style={{fontSize:20, fontWeight:'600', textAlign:"center"}}>Order Dispatched</Text>
                    </View>
                            
                            <View style={styles.DeliveryBoxRight}>
                            <Card style={{borderRadius:15, overflow:"hidden"}}>
                                <CardItem header bordered>
                                    <Text style={{fontWeight:'bold'}}>Order Details (#2405)</Text>
                                </CardItem>

                                <CardItem bordered>
                                    <Content>
                                        <List>
                                        <ListItem thumbnail>
                                            <Left>
                                            <Text>Peperoni Pizza</Text>
                                            </Left>
                                            <Body>
                                            <Text style={{color:'#D60B2B'}}>x2</Text>
                                            </Body>
                                            <Right>
                                            <Text style={{color:'#D60B2B', fontWeight:'bold'}}>N30,000</Text>
                                            </Right>
                                        </ListItem>
                                        </List>

                                        <List>
                                        <ListItem thumbnail>
                                            <Left>
                                            <Text>Peperoni Pizza</Text>
                                            </Left>
                                            <Body>
                                            <Text style={{color:'#D60B2B'}}>x2</Text>
                                            </Body>
                                            <Right>
                                            <Text style={{color:'#D60B2B', fontWeight:'bold'}}>N10,000</Text>
                                            </Right>
                                        </ListItem>
                                        </List>

                                        <List>
                                        <ListItem thumbnail>
                                            <Left>
                                            <Text>Peperoni Pizza</Text>
                                            </Left>
                                            <Body>
                                            <Text style={{color:'#D60B2B'}}>x2</Text>
                                            </Body>
                                            <Right>
                                            <Text style={{color:'#D60B2B', fontWeight:'bold'}}>N10,000</Text>
                                            </Right>
                                        </ListItem>
                                        </List>
                                    </Content>
                                </CardItem>
                                </Card>

                                <List>
                                    <ListItem thumbnail>
                                        <Left>
                                        <Text style={{fontWeight:'bold'}}>Total:</Text>
                                        </Left>
                                        <Body style={{float:"right"}}>
                                        <Text style={{color:'#D60B2B', fontWeight:'bold'}}>N10,000</Text>
                                        </Body>
                                    </ListItem>
                                </List>

                                <View style={styles.orderInfo}>
                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:"bold"}}>Payment Mode:</Text>
                                    <Text>Payment on Delivery</Text>
                                </View>

                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:"bold"}}>Date Ordered:</Text>
                                    <Text>22/10/2018</Text>
                                </View>

                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:"bold"}}>Order Status:</Text>
                                    <Text style={{color:"#D60B2B"}}>Completed</Text>
                                </View>

                                <View style={styles.infoDetails}>
                                    <Text style={{fontWeight:"bold"}}>Shipping Address:</Text>
                                    <Text>123, Lagos Street, Lagos State, Nigeria</Text>
                                </View>

                            </View>
                            </View>

                    <View style={{flexDirection:'row', alignContent:'center', justifyContent:'center', backgroundColor:'#f3f3f3', paddingBottom:30, width:'100%'}}>
                        <TouchableOpacity style={styles.btn} onPress={() => {navigate('Deliveries')}}>
                            <Text style={{color:'#fff', alignSelf:'center',marginLeft: 20, marginRight: 20}}>VIEW ALL ORDERS</Text>
                        </TouchableOpacity>
                    </View> 

                </View>
            </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    DeliveryBoxLeft:{
        padding:20,
        marginTop:10,
        backgroundColor:'#F3F3F3'
    },
    DeliveryBoxRight:{
        padding:20,
        backgroundColor: '#F3F3F3',
    },
    orderInfo:{
        margin:10,
    },
    infoDetails:{
        marginBottom:20,
    },
    DeliveryInfo:{
        flexDirection:'row',
        width:'100%',
        textAlign:"center",
        padding:15,
        backgroundColor:'#fff',
        borderBottomColor: '#cccccc',
        borderTopColor: '#ffffff',
        borderLeftColor: '#ffffff',
        borderRightColor: '#ffffff',
        borderWidth: 1,
    },
    DeliveryInfoTrigger:{
        flexDirection:'column',
        textAlign:"center"
    },
    DeliveryTriggerStart:{
        padding:10,
        paddingLeft:20,
        paddingRight:20,
        backgroundColor:'#C00A27',
        borderRadius:50,
        marginLeft:30,
    },
    DeliveryStatus:{
        textAlign:"center",
        alignItems:'center',
        padding:12,
        borderBottomColor: '#cccccc',
        borderTopColor: '#ffffff',
        borderLeftColor: '#ffffff',
        borderRightColor: '#ffffff',
        borderWidth: 1,
        width:'100%',
        flex:1,
        backgroundColor:"#ffffff"
    },
    MainBg:{
        backgroundColor:'#f1f1f1',
        flex:1,
    },
    Container:{
        flex:1,
        alignItems:'center',
        marginBottom:10
    },
    PickerSize:{
      width: 250,
      paddingTop:10,
      paddingBottom:10,
      marginLeft:5,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      borderWidth:2,
      borderColor:'#C00A27',
      borderRadius:3,
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.10,
      shadowRadius: 13.16,
      elevation: 10,
      borderBottomColor: '#ffffff',
    },
    ProductImageContainer:{
      marginBottom:10,
      alignItems:'center',
      flex:1,
    },
    ProductImage:{
        marginBottom:20,
        marginTop:25,
        width:100,
        height:100,
        resizeMode:'contain'
    },
    btn:{
      textAlign:'center',
      padding:15,
      backgroundColor:'#C00A27',
      borderRadius:50,
      width:'70%'
  },
    buttoncounter:{
      borderColor:'#C00A27', 
      borderRadius:50,
      paddingLeft: 5, 
      paddingRight: 5, 
      borderWidth: 2,
      marginLeft: 'auto',
      flexDirection:'row',
      paddingTop:10,
      height:45
  },
  countext:{
    color:'#D60B2B',
    fontSize: 20,
    paddingLeft:15,
    paddingRight:15,
},
countexts:{
    color:'#C00A27',
    fontSize: 18,
    marginLeft: 10,
    marginRight:10,
},
});
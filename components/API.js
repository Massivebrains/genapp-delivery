//const BASE_URL = 'http://39ba1783.ngrok.io/genesis/api';
const BASE_URL = 'http://utosauces.com/store/api';

module.exports = {

    get: (endpoint = '', token = '') => {

        return new Promise(async (resolve, reject) => {

            let payload = {

                headers: {

                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            };

            fetch(`${BASE_URL}/${endpoint}`, payload).then(response => response.json()).then(json => {

                resolve(json);

            }).catch(error => {

                reject(error);

            });
        });

    },

    post: (endpoint = '', data = {}, token = '') => {

        return new Promise(async (resolve, reject) => {

            let payload = {

                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(data),
            };

            fetch(`${BASE_URL}/${endpoint}`, payload).then(response => response.json()).then(json => {

                resolve(json);

            }).catch(error => {

                reject(error);

            });
        });
    }
}
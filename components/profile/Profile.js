import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Icon } from 'native-base';
import DrawerMenu from '../../controllers/DrawerMenu';
import Local from '../Local';

export default class Profile extends Component {

    static navigationOptions = { drawerLabel: 'Profile' }; 
    
    constructor(props) {

      super(props);

      this.state = { user: {} };
  }

  async componentDidMount() {

      this.init();
  }
  init = async () => {

    try {

        let user = await Local.get('user');
        if(user == undefined) this.props.navigation.navigate('Login');

        this.setState({user : user});

    } catch (ex) {

        Utils.toast();
        this.props.navigation.navigate('Login');
    }
    

}
  
  render() {
      
    return(
        <View style={styles.MainBg}>

            <Header statusBarProps={{ barStyle: 'light-content' }} outerContainerStyles={{ backgroundColor: '#D60B2B' }} leftComponent={ <DrawerMenu navigation={ this.props.navigation }/>} centerComponent={{ text: 'Profile', style: { color: '#fff' } }} />

          <View style={{flex:1, margin:10}}>

            <ScrollView>

                <View style={styles.userAvatar}>
                  <Image source={this.state.user.image ? this.state.use.image : require('../../src/user.png')} style={{width: 120, height:120}} />
                </View>

                <Text style={{textAlign:"center", fontSize:18, fontWeight:'700', marginBottom:20}}>{this.state.user.name}</Text>
                <View style={{marginBottom:10,}}>
                  <Text style={styles.ProfileTitle}>Status: <Text style={styles.ProfileDetail}>{this.state.user.rider_status}</Text></Text>
                </View>

                <View style={{marginBottom:10}}>
                  <Text style={styles.ProfileTitle}>Full Name: <Text style={styles.ProfileDetail}>{this.state.user.name}</Text></Text>
                </View>

                <View style={{marginBottom:10}}>
                  <Text style={styles.ProfileTitle}>Email Addres: <Text style={styles.ProfileDetail}>{this.state.user.email}</Text></Text>
                </View>

                <View style={{marginBottom:10}}>
                  <Text style={styles.ProfileTitle}>Phone Number: <Text style={styles.ProfileDetail}>{this.state.user.phone}</Text></Text>
                </View>

                <View style={styles.formElementView}>
                  <TouchableOpacity style={styles.BtnElement} onPress={() => {this.props.navigation.navigate('EditProfile')}}>
                    <Text style={{color:"#ffffff"}}><Icon name="ios-person" style={{color:'#ffffff', fontSize:16}}/> UPDATE PROFILE</Text>
                  </TouchableOpacity>
                </View>

            </ScrollView>
            </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({

    ProfileTitle:{
      fontWeight:'700',
      textAlign:"center"
    },
    ProfileDetail:{
      fontWeight:'400',
      textAlign:"center"
    },
    userAvatar:{
      alignItems:'center',
      justifyContent: 'center',
      marginBottom:20,
      marginTop:10
    },
    MainBg:{
      backgroundColor:'#f1f1f1',
      flex:1,
    },
    icon: {
      width: 16,
      height: 19.03,
    },
    formElementView:{
      marginBottom:7,
      marginTop:30,
      marginLeft:30,
      marginRight: 30,
    },
    FormTexts:{
      padding:12,
      borderRadius:50,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      borderColor:'#dddddd',
      borderWidth: 2,
      marginBottom:10,
    },
    BtnElement:{
      backgroundColor:'#D60B2B',
      padding:15,
      borderRadius:50,
      textAlign:'center',
      alignItems:'center'
    },
});
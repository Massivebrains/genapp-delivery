import React, { Component } from 'react';
import { View, Text, StyleSheet, KeyboardAvoidingView, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Tab, Tabs, Form, Icon } from 'native-base';
import DrawerMenu from '../../controllers/DrawerMenu';
import API from '../API';
import Utils from '../Utils';
import Local from '../Local';

export default class EditProfile extends Component {

    static navigationOptions = { header: null, drawerLabel: () => null }

    constructor(props) {

      super(props);

      this.state = { first_name: '', last_name: '', email: '', phone: '' };
  }

    async componentDidMount(){

      try{
  
        let user = await Local.get('user');
        if(user) this.props.navigation.navigate('Deliveries');
      
      }catch(ex){
  
        console.log(ex);
      
      }
    }

  render() {
      
  var {navigate} = this.props.navigation;
    return(
      
        <View style={styles.MainBg}>

            <Header statusBarProps={{ barStyle: 'light-content' }} outerContainerStyles={{ backgroundColor: '#D60B2B' }} leftComponent={ <DrawerMenu navigation={ this.props.navigation }/>} centerComponent={{ text: 'Edit Profile', style: { color: '#fff' } }} />

        <Tabs tabBarUnderlineStyle={{borderBottomWidth:5, borderColor:'#D60B2B'}} tabStyle={{backgroundColor: '#D60B2B', marginBottom: 10,}} >
        <Tab heading="Basic" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#D60B2B'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#D60B2B', fontWeight: '700'}}>
          
          <ScrollView>
            <KeyboardAvoidingView behavior="position">
              
              <View style={{flex:1, margin:10, marginLeft:30, marginRight:30}}>
                <Text style={{fontSize:16, fontWeight:"700", marginTop:15}}>Basic Information</Text>
                
                <Form style={{marginTop:15}}>
                    
                    <View style={styles.formElementView}>
                      <TextInput style={styles.FormTexts} placeholder="Address"/>
                    </View>

                    <View style={styles.formElementView}>
                      <TextInput style={styles.FormTexts} placeholder="House Number"/>
                    </View>

                    <View style={styles.formElementView}>
                      <TextInput style={styles.FormTexts} placeholder="Apartment"/>
                    </View>

                    <View style={styles.formElementView}>
                      <TextInput style={styles.FormTexts} placeholder="Others"/>
                    </View>

                    <View style={styles.formElementView}>
                      <TouchableOpacity style={styles.BtnElement} onPress={() => {alert('This updates the profile')}}>
                        <Text style={{color:"#ffffff"}}><Icon name="ios-checkmark-circle" style={{color:'#ffffff', fontSize:16}}/> UPDATE PROFILE</Text>
                      </TouchableOpacity>
                    </View>

                </Form>
              </View>
              </KeyboardAvoidingView>
          </ScrollView>

          </Tab>
          <Tab heading="Password" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#D60B2B'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#D60B2B', fontWeight: '700'}}>
          
          <ScrollView>
            <KeyboardAvoidingView behavior="position">
              
              <View style={{flex:1, margin:10, marginLeft:30, marginRight:30}}>
                <Text style={{fontSize:16, fontWeight:"700", marginTop:15}}>Password</Text>
                
                <Form style={{marginTop:15}}>

                    <View style={styles.formElementView}>
                      <TextInput style={styles.FormTexts} placeholder="Current Password"/>
                    </View>

                    <View style={styles.formElementView}>
                      <TextInput style={styles.FormTexts} placeholder="New Password"/>
                    </View>

                    <View style={styles.formElementView}>
                      <TextInput style={styles.FormTexts} placeholder="Confirm Password"/>
                    </View>

                    <View style={styles.formElementView}>
                      <TouchableOpacity style={styles.BtnElement} onPress={() => {alert('This updates the password')}}>
                        <Text style={{color:"#ffffff"}}><Icon name="ios-checkmark-circle" style={{color:'#ffffff', fontSize:16}}/> UPDATE PASSWORD</Text>
                      </TouchableOpacity>
                    </View>

                </Form>
              </View>
              </KeyboardAvoidingView>
          </ScrollView>
          </Tab>
        </Tabs>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    MainBg:{
      backgroundColor:'#f1f1f1',
      flex:1,
    },
    icon: {
      width: 16,
      height: 19.03,
    },
    formElementView:{
      marginBottom:7
    },
    FormTexts:{
      padding:13,
      borderRadius:50,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.10,
      shadowRadius: 13.15,
      elevation: 2,
      marginBottom:10,
    },
    BtnElement:{
      backgroundColor:'#D60B2B',
      padding:15,
      borderRadius:50,
      textAlign:'center',
      alignItems:'center'
    },
});
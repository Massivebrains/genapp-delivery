import React from 'react';
import { StyleSheet, Image, View, Text, KeyboardAvoidingView, TextInput, TouchableOpacity } from 'react-native';

export default class ForgotPassword extends React.Component {
  
 static navigationOptions = {
   header: null,
   drawerLabel: () => null
}

    render() {
      var {navigate} = this.props.navigation;
        return (
          <View style={styles.container}>
            <KeyboardAvoidingView behavior="position">
            <View style={styles.imageContainer}>
              <Image style={ styles.imageStyle} source={require('../../src/delivery-bike.png')} />
            </View>
        
        <View style={styles.topTextContainer}>
          <Text style={{fontWeight:'600', fontSize:18, marginTop:10, marginBottom:5}}>Forgot Password</Text>
        </View>
          <View style={styles.formElementView}>
            <TextInput style={styles.FormTexts} placeholder="Enter Your User ID"/>
          </View>
        <View style={styles.formElementView}>
            <TouchableOpacity style={styles.BtnElement} onPress={() => {alert('This will send a reset code')}}>
              <Text style={styles.BtnText}>RESET PASSWORD</Text>
            </TouchableOpacity>
        </View>
        <View>
        <TouchableOpacity onPress={() => {navigate('Login')}}>
          <Text style={{color:'#D60B2B', textAlign:'center', marginTop:20, fontWeight:'600'}}>Back To Login</Text>
        </TouchableOpacity>
        </View>
        </KeyboardAvoidingView>

        <View style={styles.imageMark}>
          <Image source={require('../../src/favicon.png')} style={{width:30, height:30, resizeMode:'contain'}}/></View>
        </View>
        );
    }
}


const styles = StyleSheet.create({
  container:{
    flex:1,
    padding:20,
    backgroundColor:'#f1f1f1'
  },
  FormTexts:{
      padding:13,
      borderRadius:50,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.10,
      shadowRadius: 13.15,
      elevation: 2,
      marginBottom:10,
    },
    BtnElement:{
      backgroundColor:'#D60B2B',
      padding:15,
      borderRadius:50,
      textAlign:'center',
      alignItems:'center'
    },
  MainBg:{
      backgroundColor:'#f3f3f3',
      flex:1,
  },
  icon: {
      width: 15,
      height: 20.36,
    },
    BtnText:{
      color:'#ffffff',
    },
    imageContainer: {
      alignItems:'center',
      marginTop:'20%'
    },
    imageStyle:{
      width:110,
      height:110,
      resizeMode:'contain'
    },
    imageMark:{
      position:'absolute',
      bottom:10,
      alignItems:'center',
      flex:1,
      alignSelf: 'center' 
    },
    formElementView: {
      paddingLeft:10,
      paddingRight:7,
      paddingBottom:7
    },
    topTextContainer: {
      alignItems:'center',
      paddingTop:15,
      paddingBottom:20,
      paddingLeft:7,
      paddingRight:7
    },
    LoginText:{
      textAlign:'center',
      fontSize:16,
    },
});
import React from 'react';
import { StyleSheet, Image, View, Text, KeyboardAvoidingView, TextInput, TouchableOpacity, Alert } from 'react-native';
import API from '../API';
import Utils from '../Utils';
import Local from '../Local';

export default class Login extends React.Component {

  static navigationOptions = {  header:null, drawerLabel: () => null };

  constructor(props) {

      super(props);

      this.state = {
          email: '',
          password: '',
          editable: true,
          button: 'Log In'
      };
  }

  async componentDidMount(){

    try{

      let user = await Local.get('user');
      if(user) this.props.navigation.navigate('Deliveries');
    
    }catch(ex){

      console.log(ex);
    }
  }
  
  login = async () => {

    try {

        this.setState({ button: 'Requesting...', editable: false });

        let response = await API.post('login', { email: this.state.email, password: this.state.password, role : 'rider' });

        if (response.status == true) {

          await Local.save('user', response.data);

          Utils.toast('Login Successful.');

          this.props.navigation.navigate('Deliveries');

        } else {

            this.setState({ button: 'Log In', editable: true, password: '' });
            Alert.alert('Login', response.data);
        }

    } catch (ex) {

        this.setState({ button: 'Log In', editable: true, password: '' });
        Alert.alert('Login', 'Could not authenticate.');
    }

}
  
    render() {

      var {navigate} = this.props.navigation;
      
        return (

          <View style={styles.container}>

            <KeyboardAvoidingView behavior="position">
            
            <View style={styles.imageContainer}>
              <Image style={ styles.imageStyle} source={require('../../src/delivery-bike.png')} />
            </View>
        
            <View style={styles.topTextContainer}>
              <Text style={{fontWeight:'600', fontSize:18, marginTop:10, marginBottom:5}}>Login</Text>
            </View>

              <View style={styles.formElementView}>
                <TextInput style={styles.FormTexts} placeholder="User Email" onChangeText={email => this.setState({ email: email })} editable={this.state.editable}/>
              </View>

              <View style={styles.formElementView}>
                <TextInput style={styles.FormTexts} placeholder="Password" onChangeText={password => this.setState({ password: password })} secureTextEntry={true} editable={this.state.editable}/>
              </View>

              <View style={styles.formElementView}>
                  <TouchableOpacity style={styles.BtnElement} onPress={this.login}>
                    <Text style={styles.BtnText}>{this.state.button}</Text>
                  </TouchableOpacity>
              </View>

              <View>
                <TouchableOpacity onPress={() => {navigate('ForgotPassword')}}>
                  <Text style={{color:'#D60B2B', textAlign:'center', marginTop:20, fontWeight:'600'}}>Forgot Password?</Text>
                </TouchableOpacity>
              </View>

            </KeyboardAvoidingView>

            <View style={styles.imageMark}>
              <Image source={require('../../src/favicon.png')} style={{width:30, height:30, resizeMode:'contain'}} />
            </View>

          </View>
        );
    }
}


const styles = StyleSheet.create({

  container:{
    flex:1,
    padding:20,
    backgroundColor:'#f1f1f1'
  },
  imageStyle:{
    width:110,
    height:110,
    resizeMode:'contain'
  },
  FormTexts:{
      padding:13,
      borderRadius:50,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.10,
      shadowRadius: 13.15,
      elevation: 2,
      marginBottom:10,
    },
    BtnElement:{
      backgroundColor:'#D60B2B',
      padding:15,
      borderRadius:50,
      textAlign:'center',
      alignItems:'center'
    },
  MainBg:{
      backgroundColor:'#f3f3f3',
      flex:1,
  },
  icon: {
      width: 20,
      height: 20.05,
    },
    BtnText:{
      color:'#ffffff',
    },
    imageContainer: {
      alignItems:'center',
      marginTop:'20%'
    },
    imageMark:{
      position:'absolute',
      bottom:10,
      alignItems:'center',
      flex:1,
      alignSelf: 'center' 
    },
    formElementView: {
      paddingLeft:10,
      paddingRight:7,
      paddingBottom:7
    },
    topTextContainer: {
      alignItems:'center',
      paddingTop:15,
      paddingBottom:20,
      paddingLeft:7,
      paddingRight:7
    },
    LoginText:{
      textAlign:'center',
      fontSize:16,
    },
});
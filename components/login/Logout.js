import React from 'react';
import Utils from '../Utils';
import Local from '../Local';

export default class Login extends React.Component {

  constructor(props) {

      super(props);
  }

    async componentDidMount(){

        try {

            await Local.logout();

            this.props.navigation.navigate('Login');

        } catch (ex) {

            Utils.toast('An error occured');
        }
    }
  
    render() {
              
        return null;
    }
}
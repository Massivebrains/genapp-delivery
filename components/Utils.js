import { ToastAndroid } from 'react-native';

module.exports = {

    toast: (text = 'Could not connect at this time') => ToastAndroid.showWithGravity(text, ToastAndroid.SHORT, ToastAndroid.BOTTOM)
}
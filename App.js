import React from 'react';
import { Root } from "native-base";
import AppDrawernavigation from './controllers/Drawernavigation';


export default class App extends React.Component {
  render() {
    return (
        <Root>
            <AppDrawernavigation/>
        </Root>
    );
  }
}

